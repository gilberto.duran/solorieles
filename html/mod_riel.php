<?php
  if(isset($_POST['getCon'])){
    include('../php/conexion.php');
    $bdd=new conexion();

    $retornable=$bdd->buscarRielID($_POST['getCon']);
    $json=array(
      'id' =>$retornable['id'],
      'Nombre'=>$retornable['Nombre'],
      'Marca'=>$retornable['idMarca'],
      'Seller'=>$retornable['idSeller'],
      'Precio'=>$retornable['Precio'],
      'Talla'=>$retornable['Talla'],
      'img'=>$retornable['img'],
      'Descripcion'=>$retornable['Descripcion'],
      'Materiales'=>$retornable['Materiales'],
      'Disponibilidad'=>$retornable['disponibilidad']
    );
  }else if(isset($_POST['getAll'])){
    include('../php/conexion.php');
      $bdd=new conexion();
      $retornable=$bdd->buscarRiel($_POST['getAll']);
        $json=array(
          'id' =>$retornable['id'],
          'Nombre'=>$retornable['Nombre'],
          'Marca'=>$retornable['idMarca'],
          'Seller'=>$retornable['idSeller'],
          'Precio'=>$retornable['Precio'],
          'Talla'=>$retornable['Talla'],
          'img'=>$retornable['img'],
          'Descripcion'=>$retornable['Descripcion'],
          'Materiales'=>$retornable['Materiales'],
          'Disponibilidad'=>$retornable['disponibilidad']
        );
      }
        $print='<div id="contenedor_registro" class="contenedor-formulario">
        <div class="input-group avatar">
            <img id="avatar" src="'.$json['img'].'">
        </div>
          <form action="mod_fin.php" class="formulario" name="formulario_registro" id="formulario_registro" method="post" enctype="multipart/form-data">
          <input type="hidden" value="'.$json['id'].'" name="idRiel">
          <input type="hidden" value="'.$json['img'].'" name="imgRiel">
            <div class="input-group">
              <input type="file" name="riel">
              <label class="label" for="nombres">Imagen del riel:</label>
            </div>

            <div class="input-group">
              <input type="text" name="nombre" id="nombre" required value="'.$json['Nombre'].'">
              <label class="label" for="nombre">Nombre del par:</label>
            </div>

            <div class="input-group">
              <label class="labelSelect" id="label3" for="image">Escoge la marca:</label>
              <select id="marca" name="marca">';
              $retornable=$bdd->getMarca();
              while($r=mysqli_fetch_array($retornable)){
                if($r['id']==$json['Marca']){
                  $print.="<option value=".$r['id']." selected>".$r['Nombre'].'</option>';
                }else{
                  $print.="<option value=".$r['id'].">".$r['Nombre'].'</option>';
                }
              }
              $print.='</select>
            </div>

            <div class="input-group">
              <label class="labelSelect" id="label3" for="image">Escoge al seller:</label>
              <select id="seller" name="seller" disabled="disabled">';
              $retornable=$bdd->getSeller();
              while($r=mysqli_fetch_array($retornable)){
                if($r['id']==$json['Seller']){
                  $print.="<option value=".$r['id']." selected>".$r['Nombre'].'</option>';
                }else{
                  $print.="<option value=".$r['id'].">".$r['Nombre'].'</option>';
                }
              }
              $print.='</select>
            </div>

            <div class="input-group">
              <input type="number" step="0.01" min=0 name="precio" id="precio" value="'.$json['Precio'].'" required>
              <label class="label" for="precio">Precio:</label>
            </div>

            <div class="input-group">
              <input type="number" name="talla" step="0.5" min=1 id="talla" value="'.$json['Talla'].'" required>
              <label class="label" for="talla">Talla:</label>
            </div>

            <div class="input-group">
              <textarea name="descripcion" rows="8" cols="80" value="" required>'.$json['Descripcion'].'</textarea>
              <label class="label" for="descripcion">Descripcion:</label>
            </div>

            <div class="input-group">
              <textarea name="materiales" rows="8" cols="80" value="" required>'.$json['Materiales'].'</textarea>
              <label class="label" for="materiales">Materiales:</label>
            </div>

            <div class="input-group">
              <input type="number" name="disponibilidad" min=1 id="disponibilidad" value="'.$json['Disponibilidad'].'" required>
              <label class="label" for="disponibilidad">Disponibilidad:</label>
            </div>

            <input type="submit" name="rielMOD" id="modificar" value="Registrar">
          </form>
        </div>';
        echo $print;
 ?>
