<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>Solo Rieles - Registro</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../css/modificar_datos.css">
    <link rel="stylesheet" href="../pluggins/mensaje.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
    <script src='../js/jquery-3.4.1.min.js' charset='utf-8'></script>
    <script src="../pluggins/mensaje.min.js"></script>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#hidden").val(sessionStorage.getItem('type'));
      })
    </script>
</head>
<body>
  <?php
    if(isset($_POST['MOD'])){
      include '../php/conexion.php';
      $bdd=new conexion();
      $bdd->registrar($_POST['nombres'], $_POST['apellidos'], $_POST['correo'], $_POST['usuario'], $_POST['pass'], $_POST['telefono'], $_POST['direccion'], $_POST['dui'], $_POST['avatar'], $_POST['usertype']);

      echo "<script type='text/javascript'>
      Swal.fire({
          type: 'success',
          title: 'Registrado con exito',
          text: 'Tenemos un nuevo miembro de la familia',
      }).then(function(){
        window.history.go(-2);
      });</script>";
    }
   ?>
    <header>
        <h1>Registrar</h1>
    </header>
    <div class="contenedor">
        <div class="wrap">
            <div id="contenedor_registro" class="contenedor-formulario">
                <form action="newUser.php" class="formulario" name="formulario_registro" id="formulario_registro" method="post">
                <input type="hidden" id='hidden' name="usertype">
                    <div class="input-group">
                        <input type="text" name="nombres" id="nombres"  required>
                        <label class="label" for="nombres">Nombres:</label>
                    </div>

                    <div class="input-group">
                        <input type="text" name="apellidos" id="apellidos"  required>
                        <label class="label" for="apellidos">Apellidos:</label>
                    </div>

                    <div class="input-group">
                        <input type="email" name="correo" id="correo" required>
                        <label class="label" for="correo">Correo:</label>
                    </div>

                    <div class="input-group">
                        <input type="text" name="usuario" id="usuario"  required>
                        <label class="label" for="usuario">Usuario:</label>
                    </div>

                    <div class="input-group">
                        <input type="text" name="direccion" id="direccion"  required>
                        <label class="label" for="direccion">Direccion:</label>
                    </div>

                    <div class="input-group">
                        <input type="password" name="pass" id="pass"  required>
                        <label class="label" for="pass">Password:</label>
                    </div>

                    <div class="input-group">
                        <input type="text" name="dui" id="dui"  required>
                        <label class="label" for="dui">DUI:</label>
                        <label class="aclaracion">Formato: ########-#</label>
                    </div>

                    <div class="input-group">
                        <input type="text" name="telefono" id="telefono"  required>
                        <label class="label" for="telefono">Teléfono:</label>
                        <label class="aclaracion">Formato: 0000-0000</label>
                    </div>

                    <div class="input-group">
                        <label class="labelSelect" id="label3" for="image">Escoge un avatar:</label>
                        <select id="image" name="avatar">
                          <option value="img/ninja.png">Ninja</option>
                          <option value="img/peliroja.png">Mujer</option>
                          <option value="img/mesero.png">Mesero</option>
                        </select>
                    </div>

                    <div class="input-group avatar">
                        <img id="avatar" src="../img/ninja.png">
                    </div>

                    <input type="submit" name="MOD" id="modificar" value="Registrar">
                </form>
            </div>
        </div>
    </div>

    <script src="../js/logicaModificarDatos.js"></script>
    <footer>
        <h3>Solo Rieles</h3>
    </footer>
</body>
</html>
