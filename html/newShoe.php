<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <title>Solo Rieles - Registro</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../css/modificar_datos.css">
  <link rel="stylesheet" href="../pluggins/mensaje.min.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
  <script src='../js/jquery-3.4.1.min.js' charset='utf-8'></script>
  <script src="../pluggins/mensaje.min.js"></script>
</head>
<body>
  <?php
    if(isset($_POST['MOD'])){
      include('../php/conexion.php');
      $riel='';
      if (($_FILES['riel']["type"] == "image/pjpeg")
      || ($_FILES['riel']["type"] == "image/jpeg")
      || ($_FILES['riel']["type"] == "image/png")
      || ($_FILES['riel']["type"] == "image/gif")) {
          if (move_uploaded_file($_FILES["riel"]["tmp_name"], "../img/".$_FILES['riel']['name'])) {
          //more code here...
             $riel="../img/".$_FILES['riel']['name'];
             $bdd=new conexion();

             if($bdd->newRiel($_POST['nombre'],$_POST['marca'],$_POST['seller'], $_POST['precio'],$_POST['talla'],$riel,$_POST['descripcion'], $_POST['materiales'],$_POST['disponibilidad'])){
               echo "<script type='text/javascript'>
                Swal.fire({
                  type: 'success',
                  title: 'Nuevo riel',
                  text: 'Grasa nueva para la raza',
                }).then(function(){
                  window.history.go(-2);
                });</script>
               ";
             }else {
               echo "error";
             }
          } else {
            echo 0;
          }
        }

    }
   ?>
  <header>
    <h1>Registrar</h1>
  </header>
  <div class="contenedor">
    <div class="wrap">
      <div id="contenedor_registro" class="contenedor-formulario">
        <form action="newShoe.php" class="formulario" name="formulario_registro" id="formulario_registro" method="post" enctype="multipart/form-data">
          <div class="input-group">
            <input type="file" name="riel" required>
            <label class="label" for="nombres">Imagen del riel:</label>
          </div>

          <div class="input-group">
            <input type="text" name="nombre" id="nombre" required>
            <label class="label" for="nombre">Nombre del par:</label>
          </div>

          <div class="input-group">
            <label class="labelSelect" id="label3" for="image">Escoge la marca:</label>
            <select id="marca" name="marca">
              <?php
              include('../php/conexion.php');
              $bdd=new conexion();

              $retorno=$bdd->getMarca();

              while($r=mysqli_fetch_array($retorno)){
                echo "<option value=".$r['id'].">".$r['Nombre'].'</option>';
              }
               ?>
            </select>
          </div>

          <div class="input-group">
            <label class="labelSelect" id="label3" for="image">Escoge al seller:</label>
            <select id="seller" name="seller">
              <?php
              $retorno=$bdd->getSeller();
              if(isset($_GET['username'])){
                while($r=mysqli_fetch_array($retorno)){
                  if($r['username']==$_GET['username'])
                    echo "<option value=".$r['id']." selected>".$r['Nombre'].'</option>';
                }
              }else{
                while($r=mysqli_fetch_array($retorno)){
                  echo "<option value=".$r['id'].">".$r['Nombre'].'</option>';
                }
              }
               ?>
            </select>
          </div>

          <div class="input-group">
            <input type="number" step='0.01' min=0 name="precio" id="precio" required>
            <label class="label" for="precio">Precio:</label>
          </div>

          <div class="input-group">
            <input type="number" name="talla" step='0.5' min=1 id="talla" required>
            <label class="label" for="talla">Talla:</label>
          </div>

          <div class="input-group">
            <textarea name="descripcion" rows="8" cols="80" required></textarea>
            <label class="label" for="descripcion">Descripcion:</label>
          </div>

          <div class="input-group">
            <textarea name="materiales" rows="8" cols="80"></textarea>
            <label class="label" for="materiales">Materiales:</label>
          </div>

          <div class="input-group">
            <input type="number" name="disponibilidad" min=1 id="disponibilidad" required>
            <label class="label" for="disponibilidad">Disponibilidad:</label>
          </div>

          <input type="submit" name="MOD" id="modificar" value="Registrar">
        </form>
      </div>
    </div>
  </div>

  <footer>
    <h3>Solo Rieles</h3>
  </footer>
</body>

</html>
