<?php
class conexion
{
  function login($login, $password){
    $conn = mysqli_connect('localhost','root','','solorieles');
    $query = $conn -> query ("select id, userType from cliente where username='".$login."' and Password='".$password."'");
    $i=0;
    $id=0;
    while($valores=mysqli_fetch_array($query)){
        $i++;
        $id=$valores['userType'];
    }
    mysqli_close($conn);
    if($i>0)
      return $id;
    else
      return 0;
  }

  function registrar($name, $lastname, $email, $username, $password, $telefono, $direccion, $dui, $img, $usertype='cliente'){
    $conn = mysqli_connect('localhost','root','','solorieles');

    try {
      $conn -> query("insert into cliente (`Nombres`, `Apellidos`, `Email`, `username`, `Password`, `Telefono`, `Direccion`, `DUI`, `avatar`, `userType`) values  ('".$name."','".$lastname."','".$email."','".$username."', '".$password."', '".$telefono."', '".$direccion."', '".$dui."', '".$img."', '".$usertype."')");
    } catch (Exception $e) {
      echo "<script language='javascript'>console.log($e)</script>";
    }


    mysqli_close($conn);
  }

  function getVentas(){
    $conn = mysqli_connect('localhost','root','','solorieles');

    $query=$conn->query("select dc.cantidad, c.fecha, c.Monto, r.Nombre, r.img, concat(cli.Nombres,' ',cli.Apellidos) as 'Nombres', concat(cl.Nombres,' ', cl.Apellidos) as 'Comprador' from detalle_compra dc inner join compra c on c.id=dc.idCompra inner join cliente cl on cl.id=c.idCliente inner join rieles r on r.id=dc.idRiel inner join cliente cli on cli.id=r.idSeller");

    $rows=array();

    while($r=mysqli_fetch_array($query)){
      $json=array(
        'img'=>$r['img'],
        'nombre'=>$r['Nombre'],
        'cantidad'=>$r['cantidad'],
        'monto'=>$r['Monto'],
        'fecha'=>$r['fecha'],
        'vendedor'=>$r['Nombres'],
        'comprador'=>$r['Comprador']
      );

      $rows[]=$json;
    }
    return $rows;
  }

  function getVentasSeller($id){
    $conn = mysqli_connect('localhost','root','','solorieles');

    $query=$conn->query("select dc.cantidad, c.fecha, c.Monto, r.Nombre, r.img, concat(cli.Nombres,' ',cli.Apellidos) as 'Nombres', concat(cl.Nombres,' ', cl.Apellidos) as 'Comprador' from detalle_compra dc inner join compra c on c.id=dc.idCompra inner join cliente cl on cl.id=c.idCliente inner join rieles r on r.id=dc.idRiel inner join cliente cli on cli.id=r.idSeller where cli.username='$id'");

    $rows=array();

    while($r=mysqli_fetch_array($query)){
      $json=array(
        'img'=>$r['img'],
        'nombre'=>$r['Nombre'],
        'cantidad'=>$r['cantidad'],
        'monto'=>$r['Monto'],
        'fecha'=>$r['fecha'],
        'vendedor'=>$r['Nombres'],
        'comprador'=>$r['Comprador']
      );

      $rows[]=$json;
    }
    return $rows;
  }

  function newRiel($nombre, $marca, $seller, $precio, $talla, $img, $descripcion, $materiales, $disponibilidad){
    $conn = mysqli_connect('localhost','root','','solorieles');
    try{
      $cadena="insert into rieles (`Nombre`, `idMarca`, `idSeller`, `Precio`, `Talla`, `img`, `Descripcion`, `Materiales`, `disponibilidad`) values ('$nombre',$marca,$seller,$precio,$talla,'$img','$descripcion', '$materiales',$disponibilidad)";
      $conn->query($cadena);
      return true;
    }catch(Exception $e){
      return false;
    }
  }

  function updateRiel($nombre, $marca, $precio, $talla, $img, $descripcion, $materiales, $disponibilidad,$id){
    $conn = mysqli_connect('localhost','root','','solorieles');
    try{
      $cadena="update rieles set Nombre='$nombre', idMarca=$marca, Precio=$precio, Talla=$talla, img='$img', Descripcion='$descripcion', Materiales='$materiales', disponibilidad=$disponibilidad where id=$id";
      $conn->query($cadena);
      return true;
    }catch(Exception $e){
      return false;
    }
  }

  function shoes(){
    $conn = mysqli_connect('localhost','root','','solorieles');
    $query = $conn -> query ('select img, Nombre, precio from rieles group by Nombre' );
    while($valores=mysqli_fetch_array($query)){
      $cantidad=$this->valCant($valores['Nombre']);
      $agotado='';
      if($cantidad==0)
        $agotado='Agotado';
      echo "
        <div class='card'>
          <img src='".$valores['img']."'>
          <h4>".$valores['Nombre']."</h4>
          <h5>$".$valores['precio']."</h5>
          <h5 class='agotado'>$agotado</h5>
          <form action='info.php' method='post'>
            <input type='hidden' value='".$valores['Nombre']."' name='riel'>
            <input name='super' type='submit' class='button' value='Ver mas'></input>
          </form>
        </div>
      ";
    }
  }

  function getRiel(){
    $conn = mysqli_connect('localhost','root','','solorieles');
    $query = $conn -> query ("select r.id, r.Nombre, m.Nombre as 'marca', concat(s.Nombres,' ',s.Apellidos) as 'Seller', Precio, Talla, img, disponibilidad from rieles r inner join marcas m on m.id=r.idMarca inner join cliente s on s.id=r.idSeller");
    $rows=array();
    while($r=mysqli_fetch_array($query)){
      $json=array(
        'id'=>$r['id'],
        'Nombre'=>$r['Nombre'],
        'marca'=>$r['marca'],
        'Seller'=>$r['Seller'],
        'Precio'=>$r['Precio'],
        'Talla'=>$r['Talla'],
        'img'=>$r['img'],
        'disponibilidad'=>$r['disponibilidad']
      );

      $rows[]=$json;
    }
    return $rows;
  }

  function getRielSeller($username){
    $conn = mysqli_connect('localhost','root','','solorieles');
    $query = $conn -> query ("select r.id, r.Nombre, m.Nombre as 'marca', concat(s.Nombres,' ',s.Apellidos) as 'Seller', Precio, Talla, img, disponibilidad from rieles r inner join marcas m on m.id=r.idMarca inner join cliente s on s.id=r.idSeller where s.username='".$username."'");
    $rows=array();
    while($r=mysqli_fetch_array($query)){
      $json=array(
        'id'=>$r['id'],
        'Nombre'=>$r['Nombre'],
        'marca'=>$r['marca'],
        'Seller'=>$r['Seller'],
        'Precio'=>$r['Precio'],
        'Talla'=>$r['Talla'],
        'img'=>$r['img'],
        'disponibilidad'=>$r['disponibilidad']
      );
      $rows[]=$json;
    }

    return $rows;
  }

  function getRielLike($riel){
    $conn = mysqli_connect('localhost','root','','solorieles');
    $query = $conn -> query ("select r.id, r.Nombre, m.Nombre as 'marca', concat(s.Nombres,' ',s.Apellidos) as 'Seller', Precio, Talla, img, disponibilidad from rieles r inner join marcas m on m.id=r.idMarca inner join cliente s on s.id=r.idSeller where r.Nombre like '%".$riel."%'");
    $rows=array();
    while($r=mysqli_fetch_array($query)){
      $json=array(
        'id'=>$r['id'],
        'Nombre'=>$r['Nombre'],
        'marca'=>$r['marca'],
        'Seller'=>$r['Seller'],
        'Precio'=>$r['Precio'],
        'Talla'=>$r['Talla'],
        'img'=>$r['img'],
        'disponibilidad'=>$r['disponibilidad']
      );

      $rows[]=$json;
    }
    return $rows;
  }

  function getRielLikeSeller($riel,$seller){
    $conn = mysqli_connect('localhost','root','','solorieles');
    $query = $conn -> query ("select r.id, r.Nombre, m.Nombre as 'marca', concat(s.Nombres,' ',s.Apellidos) as 'Seller', Precio, Talla, img, disponibilidad from rieles r inner join marcas m on m.id=r.idMarca inner join cliente s on s.id=r.idSeller where r.Nombre like '%".$riel."%' and s.username='$seller'");
    $rows=array();
    while($r=mysqli_fetch_array($query)){
      $json=array(
        'id'=>$r['id'],
        'Nombre'=>$r['Nombre'],
        'marca'=>$r['marca'],
        'Seller'=>$r['Seller'],
        'Precio'=>$r['Precio'],
        'Talla'=>$r['Talla'],
        'img'=>$r['img'],
        'disponibilidad'=>$r['disponibilidad']
      );

      $rows[]=$json;
    }
    return $rows;
  }

  function shoesSearch($rielname){
    $conn = mysqli_connect('localhost','root','','solorieles');
    if($rielname=='---')
      $query=$conn->query('select img, Nombre, precio from rieles group by Nombre');
    else
      $query = $conn -> query ('select img, Nombre, precio from rieles where Nombre like \'%'.$rielname.'%\' group by Nombre');

    return $query;
  }

  function buscarRiel($nombre){
    $conn = mysqli_connect('localhost','root','','solorieles');
    $query = $conn -> query ("select * from rieles where Nombre='$nombre'");

    $valores=mysqli_fetch_array($query);

    return $valores;
  }

  function buscarRielID($id){
    $conn = mysqli_connect('localhost','root','','solorieles');
    $query = $conn -> query ("select * from rieles where id='$id'");

    $valores=mysqli_fetch_array($query);

    return $valores;
  }

  function buscarRielbyTalla($nombre, $talla){
    $conn = mysqli_connect('localhost','root','','solorieles');
    $query = $conn -> query ("select * from rieles where Nombre='$nombre' and Talla='$talla'");

    $rows=array();
    while($r=mysqli_fetch_array($query)){
      $json=array(
        'id' => utf8_encode($r['id']),
        'Nombre' => utf8_encode($r['Nombre']),
        'Talla' => utf8_encode($r['Talla']),
        'img' => utf8_encode($r['img']),
        'Precio' => utf8_encode($r['Precio'])
      );
      $rows[]=$json;
    }
    header("Content-type:application/json");
    return $rows;

    /*$valores=mysqli_fetch_array($query);
    return $valores;*/
  }

  function getTallas($nombre){
    $conn = mysqli_connect('localhost','root','','solorieles');
    $query = $conn -> query ("select Talla from rieles where Nombre='$nombre' and disponibilidad>=1");

    while($valores=mysqli_fetch_array($query)){
      echo "<option value='".$valores['Talla']."'>".$valores['Talla']."</option>";
    }

    mysqli_close($conn);

  }

  function getCantidad($nombre){
    echo "<h3>Disponibles: ".$this->valCant($nombre)."</h3>";
  }

  function valCant($nombre){
    $conn = mysqli_connect('localhost','root','','solorieles');
    $query = $conn -> query ("select disponibilidad from rieles where Nombre='$nombre'");
    $dis=0;
    while($valores=mysqli_fetch_array($query)){
      $dis+=$valores['disponibilidad'];
    }
    return $dis;
  }

  function buscarUsuario($username){
    $conn = mysqli_connect('localhost','root','','solorieles');
    $query = $conn -> query ("select * from cliente where username='$username'");

    $valores=mysqli_fetch_array($query);
    mysqli_close($conn);
    return $valores;
  }

  function updateUsuario($cadena){
    $conn=mysqli_connect('localhost','root','','solorieles');
    try{
      $conn->query($cadena);
      return true;
    }catch(Exception $e){
      return false;
    }
  }

  function compraUsuario($username,$monto){
    $conn=mysqli_connect('localhost','root','','solorieles');
    $val=$this->buscarUsuario($username);
    $fecha=date("Y-m-d");
    try{
      $conn->query("insert into compra (`idCliente`, `Fecha`, `Monto`) values(".$val['id'].", '$fecha', $monto)");
      return true;
    }catch(Exception $e){
      return false;
    }
  }

  function compraRiel($rielname){
    //$riel=$this->getRielTop($rielname);
    $conn=mysqli_connect('localhost','root','','solorieles');
    try{
      $query=$conn->query("select id from compra ORDER by id desc limit 1");
      $idComp=mysqli_fetch_array($query);
      $conn->query("insert into detalle_compra (`idCompra`, `idRiel`, `cantidad`) values(".$idComp['id'].", ".$rielname.", 1)");
      mysqli_close($conn);
      return true;
    }catch(Exception $e){
      mysqli_close($conn);
      return false;
    }
  }

  function getRielTop($rielname){
    $exe="select id from rieles where Nombre='$rielname' and disponibilidad>=1 limit 1";
    $conn=mysqli_connect('localhost','root','','solorieles');
    $query = $conn -> query ($exe);
    $valores=mysqli_fetch_array($query);
    mysqli_close($conn);
    return $valores;
  }

  function reduce($id){
    $conn=mysqli_connect('localhost','root','','solorieles');
    $query=$conn->query("select disponibilidad from rieles where id=".$id);
    $dis=mysqli_fetch_array($query);
    $conn->query("update rieles set disponibilidad=".($dis['disponibilidad']-1)." where id=".$id);
  }

  function addDis($id){
    $conn=mysqli_connect('localhost','root','','solorieles');
    try{
      $query=$conn->query("select disponibilidad from rieles where id=".$id);
      $dis=mysqli_fetch_array($query);
      $conn->query("update rieles set disponibilidad=".($dis['disponibilidad']+1)." where id=".$id);
      return true;
    }catch(Exception $ex){
      return false;
    }
  }

  function autComplete($cadena){
    $conn=$conn=mysqli_connect('localhost','root','','solorieles');
    $query=$conn->query("select * from rieles where Nombre like '%$cadena%' group by Nombre ORDER BY Nombre ASC LIMIT 0, 7");

    return $query;
  }

  function autCompleteSeller($cadena){
    $conn=$conn=mysqli_connect('localhost','root','','solorieles');
    $query=$conn->query("select * from rieles r inner join cliente c on c.id=r.idSeller where Nombre like '%$cadena%' and c.username='nbukele' group by Nombre ORDER BY Nombre ASC LIMIT 0, 7");

    return $query;
  }

  function userByType($usertype){
    $conn = mysqli_connect('localhost','root','','solorieles');

    $query=$conn->query('select * from cliente where userType=\''.$usertype.'\'');

    $rows=array();
    while($r=mysqli_fetch_array($query)){
      $json=array(
        'id' => utf8_encode($r['id']),
        'Nombres' => utf8_encode($r['Nombres']),
        'Apellidos' => utf8_encode($r['Apellidos']),
        'Email' => utf8_encode($r['Email']),
        'username' => utf8_encode($r['username']),
        'Telefono' => utf8_encode($r['Telefono']),
        'Direccion' => utf8_encode($r['Direccion']),
        'avatar' => utf8_encode($r['avatar'])
      );

      $rows[]=$json;
    }

    header("Content-type:application/json");
    return $rows;
  }

  function userByTypeByName($username, $usertype){
    $conn = mysqli_connect('localhost','root','','solorieles');

    $query=$conn->query('select * from cliente where username like \'%'.$username.'%\' or Nombres like \'%'.$username.'%\' or Apellidos like \'%'.$username.'%\' or concat(Nombres, \' \', Apellidos) like \'%'.$username.'%\' and usertype=\''.$usertype.'\'');

    $rows=array();
    while($r=mysqli_fetch_array($query)){
      $json=array(
        'id' => utf8_encode($r['id']),
        'Nombres' => utf8_encode($r['Nombres']),
        'Apellidos' => utf8_encode($r['Apellidos']),
        'Email' => utf8_encode($r['Email']),
        'username' => utf8_encode($r['username']),
        'Telefono' => utf8_encode($r['Telefono']),
        'Direccion' => utf8_encode($r['Direccion']),
        'avatar' => utf8_encode($r['avatar'])
      );

      $rows[]=$json;
    }

    header("Content-type:application/json");
    return $rows;
  }

  function deleteUser($id){
    $conn = mysqli_connect('localhost','root','','solorieles');
    try{
      $conn->query('delete from cliente where username=\''.$id.'\'');
      return true;
    }catch(Exception $e){
      return false;
    }
  }

  function deleteRiel($id){
    $conn = mysqli_connect('localhost','root','','solorieles');
    try{
      $conn->query('delete from riel where id='.$id);
      return true;
    }catch(Exception $e){
      return false;
    }
  }

  function getMarca(){
    $conn = mysqli_connect('localhost','root','','solorieles');
    $query=$conn->query('select * from marcas');

    return $query;
  }

  function getSeller(){
    $conn = mysqli_connect('localhost','root','','solorieles');
    $query=$conn->query('select id, concat(Nombres, \' \', Apellidos)  as \'Nombre\', username from cliente where userType=\'seller\'');

    return $query;
  }
}
 ?>
