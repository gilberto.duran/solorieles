$(document).ready(function(){
  getAll();
});

function getAll(){
  $.ajax({
    url: "../html/admins_ventas.php",
    type: "post",
    data: {getall:'all'},
    success: function(data){
      made(data);
    }
  });
}

function made(data){
  for(let i in data){
    var nodeTabla = document.getElementById('cuerpo');
    var nodeTR = document.createElement('tr');
    var nodeCantidad = document.createElement('td');
    var nodeAvatar = document.createElement('td');
    var nodeNombre = document.createElement('td');
    var nodeMonto = document.createElement('td');
    var nodeFecha = document.createElement('td');
    var nodeVendedor = document.createElement('td');
    var nodeComprador = document.createElement('td');
    var img = document.createElement('img');

    img.setAttribute('src',data[i].img);
    img.setAttribute('id', 'imgProducto');
    img.className += "zapato";
    nodeAvatar.appendChild(img);
    nodeCantidad.innerHTML=data[i].cantidad;
    nodeNombre.innerHTML=data[i].nombre;
    nodeMonto.innerHTML='$'+data[i].monto;
    nodeFecha.innerHTML=data[i].fecha;
    nodeVendedor.innerHTML=data[i].vendedor;
    nodeComprador.innerHTML=data[i].comprador;
    nodeTR.appendChild(nodeAvatar);
    nodeTR.appendChild(nodeNombre);
    nodeTR.appendChild(nodeCantidad);
    nodeTR.appendChild(nodeMonto);
    nodeTR.appendChild(nodeFecha);
    nodeTR.appendChild(nodeVendedor);
    nodeTR.appendChild(nodeComprador);
    nodeTabla.appendChild(nodeTR);
  }
}
