window.onload = init;

function init() {
    var boton = document.getElementById('comprar');

    boton.addEventListener('click', function () {
        carrito.push(document.title);
        console.log(carrito);
        sessionStorage.setItem("carrito", JSON.stringify(carrito));
        btnCarrito.innerHTML = "<img src='../img/carrito.png'></img>";
        Swal.fire({
            type: 'success',
            title: '¡Buena elección!',
            text: 'Producto agregado al carrito',
            showConfirmButton: false,
            timer: 1500
        });
    });

    var carrito = [];
    var btnCarrito = document.getElementById('carrito_btn');

    if (sessionStorage['carrito']) {
        //Si el carrito existe, se cambia el color del carrito
        btnCarrito.innerHTML = "<img src='../img/carrito.png'></img>";
        //obtengo los datos actuales del carrito y los guardo en una variable auxiliar
        //para evitar perder los datos actulaes despues de guardar un nuevo registro
        var sub = sessionStorage.getItem('carrito');
        var s = JSON.parse(sub);
        for (let i in s)
            carrito[i] = s[i];
    };

    //listener para detectar el leave del mouse
    btnCarrito.addEventListener('mouseleave', function () {
        if (sessionStorage['carrito']) {
            //Si el carrito existe, se cambia el color del carrito
            btnCarrito.innerHTML = "<img src='../img/carrito.png'></img>";
            btnCarrito.style.background = "transparent";
            btnCarrito.style.color = "#fff";
            //obtengo los datos actuales del carrito y los guardo en una variable auxiliar
            //para evitar perder los datos actulaes despues de guardar un nuevo registro
            var sub = sessionStorage.getItem('carrito');
            var s = JSON.parse(sub);
            for (let i in s)
                carrito[i] = s[i];
        } else {
            btnCarrito.innerHTML = "<img src='../img/carrito.png'></img>"
            btnCarrito.style.background = "transparent";
            btnCarrito.style.color = "#fff";
        }

    });
}
