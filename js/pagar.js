$(document).ready(function(){
  var user = sessionStorage.getItem('username');
  if (user != null) {
    $.ajax({
      type: 'POST',
      url: 'pagar.php',
      data: {
        username: user
      },
      success: function(data){
        $("#formulario_registro").html(data);
        $("#modificar").click(function(){
        var t = $("#tarjeta").val();
        var c = $("#codigo").val();
        if(t!='' && c!='')
          window.location.href='regis_compra.html';
        });
      }
    });
  }else {
    window.location.href = 'login.php';
  }
});
