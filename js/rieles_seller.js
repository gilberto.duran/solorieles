$(document).ready(function() {
  getAll();
  var id=sessionStorage.getItem('username');
  $('#btnNew').click(function(){
    window.location.href='newShoe.php?username='+id;
  })
});

function getAll(){
  var id = sessionStorage.getItem('username');
  $.ajax({
    url: "../html/seller_riel.php",
    type: "post",
    data: {getAll:'all',
          username:id},
    success: function(data){
      made(data);
    }
  });
}

function made(data) {
  for(let i in data){
    console.log(data[i]);
    var nodeTabla = document.getElementById('cuerpo');
    var nodeTR = document.createElement('tr');
    var nodeNum = document.createElement('td');
    var nodeAvatar = document.createElement('td');
    var nodeName = document.createElement('td');
    var nodeMarca = document.createElement('td');
    var nodeSeller = document.createElement('td');
    var nodePrecio = document.createElement('td');
    var nodeTalla = document.createElement('td');
    var nodeDisponibilidad = document.createElement('td');
    var nodeModificar = document.createElement('td');
    var nodeEliminar = document.createElement('td');
    var img = document.createElement('img');

    img.setAttribute('src',data[i].img);
    img.setAttribute('id', 'imgProducto');
    img.className += "zapato";
    nodeAvatar.appendChild(img);
    nodeEliminar.setAttribute('id', data[i].Nombre);
    nodeEliminar.setAttribute('class', 'borro');
    nodeModificar.setAttribute('id', data[i].id);
    nodeModificar.setAttribute('class', 'mod');
    nodeNum.innerHTML=data[i].id;
    nodeName.innerHTML=data[i].Nombre;
    nodeMarca.innerHTML=data[i].marca;
    nodeSeller.innerHTML=data[i].Seller;
    nodePrecio.innerHTML=data[i].Precio;
    nodeTalla.innerHTML=data[i].Talla;
    nodeDisponibilidad.innerHTML=data[i].disponibilidad;
    nodeEliminar.innerHTML="<img src='../img/delete.png' class='boton-delete'>";
    nodeModificar.innerHTML="<img src='../img/modificar.png' class='boton-delete'>";

    nodeTR.appendChild(nodeNum);
    nodeTR.appendChild(nodeAvatar);
    nodeTR.appendChild(nodeName);
    nodeTR.appendChild(nodeMarca);
    nodeTR.appendChild(nodeSeller);
    nodeTR.appendChild(nodePrecio);
    nodeTR.appendChild(nodeTalla);
    nodeTR.appendChild(nodeDisponibilidad);
    nodeTR.appendChild(nodeModificar);

    nodeTabla.appendChild(nodeTR);


    $(".borro").click(function(){
      id = $(this).attr("id");
      $.ajax({
        url: "../html/admin_riel.php",
        type: "post",
        data: {delete:id},
        success: function(data){
          if(data){
            window.location.href='admin_riel.html';
          }
        }
      });
    });

    $(".mod").click(function(){
      id = $(this).attr("id");
      var uSeller=sessionStorage.getItem('username');
      sessionStorage.setItem('sId',id);
      sessionStorage.setItem('type','seller');
      sessionStorage.setItem('uSeller',uSeller);
      window.location.href=('modRiel.html');
    });
  }
}

function autocompletar(){
  var min = 1;
  var palabra = $('#busqueda').val();
  var id=sessionStorage.getItem('username');
  if(palabra.length>=min){
    $.ajax({
      url: "../html/aut_complete.php",
      type: "post",
      data: {palabra:palabra,seller:id},
      success: function(data){
        $("#rieles_opc").show();
        $("#rieles_opc").html(data);
      }
    });
  }else{
      $("#rieles_opc").hide();
      var nodeTabla = document.getElementById('cuerpo');
      nodeTabla.innerHTML='';
      getAll();
  }

}

function set_item(opciones){
  $("#busqueda").val(opciones);
  $("#rieles_opc").hide();

  $.ajax({
    url: 'getRiel.php',
    type: 'post',
    data: {riel:opciones},
    success: function(data){
      $("#principal").html(data);
    }
  });
}

function buscar(){
  var buscar=$("#busqueda").val();
  $("#rieles_opc").hide();
  var id = sessionStorage.getItem('username');
  $.ajax({
    url: 'admin_riel.php',
    type: 'post',
    data: {riel:buscar, seller:id},
    success: function(data){
      var nodeTabla = document.getElementById('cuerpo');
      nodeTabla.innerHTML='';
      made(data);
    },
    error: function(){
      console.log("error");
    }
  });
}

function focused(){
  $("#rieles_opc").show();
}
