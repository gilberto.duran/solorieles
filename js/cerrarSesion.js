$(document).ready(function(){
  var btn = document.getElementById("cerrar_sesion");
  if (btn.addEventListener) {
      btn.addEventListener("click", cerrarSesion, false);
  } else if (btn.attachEvent) {
      btn.attachEvent("onclick", cerrarSesion);
  }
});

function cerrarSesion() {
    sessionStorage.clear();
    localStorage.clear();
    if(document.title=='SoloRieles'){
      window.location.href = 'index.html';
    }else{
      window.location.href = '../index.html';
    }
}
