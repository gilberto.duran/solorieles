-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-05-2020 a las 22:36:13
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `solorieles`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id` int(11) NOT NULL,
  `userType` varchar(20) NOT NULL DEFAULT 'cliente',
  `Nombres` varchar(60) NOT NULL,
  `Apellidos` varchar(60) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `username` varchar(60) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Telefono` varchar(9) NOT NULL,
  `Direccion` varchar(100) NOT NULL,
  `DUI` varchar(10) NOT NULL,
  `avatar` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id`, `userType`, `Nombres`, `Apellidos`, `Email`, `username`, `Password`, `Telefono`, `Direccion`, `DUI`, `avatar`) VALUES
(1, 'admin', 'Gilberto', 'Duran', 'gduran@gmail.com', 'gduran', 'Gilberto@17', '7686-6839', 'Av. El Olmo', '12345678-9', 'img/ninja.png'),
(2, 'seller', 'Eddy', 'Rene', 'eddy.rene@eddy.com', 'eddy', '123456789', '1234-5678', 'vbjn', '12345678-9', 'img/ninja.png'),
(3, 'cliente', 'Jose', 'Raul', 'jse.raul@g.com', 'raul', 'Raul@17', '7589-9898', 'dxcvb', '12345678-9', 'img/peliroja.png'),
(4, 'cliente', 'James', 'Rodriguex', 'jj@f.com', 'james', '123456789', '4565-9898', 'cvbhj', '12345678-9', 'img/peliroja.png'),
(6, 'cliente', 'Jose', 'Lopez', 'jlopez@g.com', 'jlopez', '123456789', '7589-9898', 'fcvb', '12345678-9', 'img/ninja.png'),
(7, 'cliente', 'Kevin', 'Duran', 'kduran@gmail.com', 'kev.durn', '123456', '1234-5678', 'Universidad Don Bosco', '12345678-9', 'img/mesero.png'),
(8, 'cliente', 'Jose', 'Casas', 'jcasas@m.com', 'jcasas', '123456', '1234-5678', 'vgbhnjmk', '12345678-9', 'img/mesero.png'),
(9, 'cliente', 'Kevin', 'DurÃ¡n', 'kevdu.m@gmail.com', 'kduran', '123456789', '10100101', 'si cu un ok un Chi in CD de', '04833820-1', 'img/mesero.png'),
(12, 'seller', 'Juan Antonio', 'Lopez Escamilla', 'jlope@gmail.com', 'jescamilla', '123456', '7898-9898', 'km. 1/2 carretera plan del pino, soyapango', '12345678-9', 'img/ninja.png'),
(13, 'seller', 'Ramon', 'Anderson', 'randerson@gmail.com', 'randerson', '123456', '8998-9999', 'Rand, 123, col. av.', '12345678-9', 'img/ninja.png'),
(14, 'seller', 'Andres', 'Robersont', 'a@mmm.com', 'arobersont', '123456', '8798-9898', 'ghjk', '12345678-9', 'img/ninja.png'),
(18, 'seller', 'Andres Manuel', 'Lopez Obrador', 'amlo@mex.com', 'amlo', '123456', '7898-9898', 'Chiapas', '12345678-9', 'img/peliroja.png'),
(26, 'seller', 'Nayib', 'Bukele', 'nbukele@rieles.com', 'nbukele', '123456', '7556-6565', 'Casa presidencial', '12345678-9', 'img/mesero.png'),
(29, 'admin', 'Walter', 'Carabantes', 'wcarabantes@gmail.com', 'wcarabantes', '123456', '7869-3636', 'Chalatenango', '12345678-9', 'img/mesero.png'),
(30, 'admin', 'Alejandro', 'Alas', 'aalas@rieles.com', 'aalas', '123456', '7356-6989', 'Soya', '12345678-9', 'img/peliroja.png'),
(31, 'seller', 'Kevin', 'Cedillo', 'kcedillos@rieles.com', 'kcedillos', '123456', '7845-3212', 'Soya', '12345678-9', 'img/ninja.png'),
(32, 'seller', 'Riley', 'Reid', 'rreid@rieles.com', 'riley.reid', '123456', '7686-6839', 'Pornhub', '12345678-9', 'img/peliroja.png'),
(33, 'seller', 'Kevin Alexander', 'Reid', 'kreid@solorieles.com', 'kevin.reid', '123456', '7989-2335', 'Cupertino, California', '12345678-9', 'img/ninja.png'),
(35, 'cliente', 'Marcela', 'Villafranca', 'mvilla@gmail.com', 'marcela.villa', '123456', '7898-9852', 'Universidad Don Bosco', '12345678-9', 'img/ninja.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra`
--

CREATE TABLE `compra` (
  `id` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL,
  `Fecha` date NOT NULL,
  `Monto` float(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compra`
--

INSERT INTO `compra` (`id`, `idCliente`, `Fecha`, `Monto`) VALUES
(36, 3, '2020-05-02', 1286.00),
(37, 3, '2020-05-02', 890.00),
(38, 3, '2020-05-02', 119.00),
(39, 3, '2020-05-02', 119.00),
(40, 3, '2020-05-02', 119.00),
(41, 3, '2020-05-02', 119.00),
(42, 3, '2020-05-02', 799.00),
(43, 3, '2020-05-02', 799.00),
(44, 3, '2020-05-02', 228.00),
(45, 3, '2020-05-02', 228.00),
(46, 3, '2020-05-02', 410.00),
(47, 3, '2020-05-02', 410.00),
(48, 3, '2020-05-02', 410.00),
(49, 3, '2020-05-02', 410.00),
(50, 3, '2020-05-02', 410.00),
(51, 3, '2020-05-02', 410.00),
(52, 3, '2020-05-02', 410.00),
(53, 4, '2020-05-06', 5228.00),
(54, 7, '2020-05-08', 260.00),
(55, 7, '2020-05-08', 260.00),
(56, 7, '2020-05-08', 260.00),
(57, 7, '2020-05-09', 369.00),
(58, 35, '2020-05-09', 693.99),
(59, 7, '2020-05-09', 1046.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_compra`
--

CREATE TABLE `detalle_compra` (
  `id` int(11) NOT NULL,
  `idCompra` int(11) NOT NULL,
  `idRiel` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_compra`
--

INSERT INTO `detalle_compra` (`id`, `idCompra`, `idRiel`, `cantidad`) VALUES
(17, 36, 7, 1),
(18, 36, 2, 1),
(19, 41, 4, 1),
(20, 42, 4, 1),
(21, 42, 7, 1),
(22, 42, 1, 1),
(23, 44, 6, 1),
(24, 45, 6, 1),
(25, 46, 7, 1),
(26, 47, 7, 1),
(27, 48, 7, 1),
(28, 49, 7, 1),
(29, 50, 7, 1),
(30, 51, 7, 1),
(31, 52, 7, 1),
(32, 53, 15, 1),
(33, 54, 16, 1),
(34, 55, 16, 1),
(35, 57, 4, 1),
(36, 57, 16, 1),
(37, 58, 3, 1),
(38, 58, 17, 1),
(39, 59, 18, 1),
(40, 59, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marcas`
--

CREATE TABLE `marcas` (
  `id` int(11) NOT NULL,
  `Nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marcas`
--

INSERT INTO `marcas` (`id`, `Nombre`) VALUES
(1, 'Nike'),
(2, 'adidas'),
(3, 'Reebok'),
(4, 'Puma'),
(5, 'Converse'),
(6, 'Skechers'),
(7, 'New Balance'),
(8, 'Asics'),
(9, 'Off-White'),
(10, 'Air Jordan'),
(11, 'Vans'),
(12, 'Fear of God'),
(13, 'Louis Vuitton'),
(14, 'Balenciaga'),
(15, 'Prada'),
(16, 'Gucci');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rieles`
--

CREATE TABLE `rieles` (
  `id` int(11) NOT NULL,
  `Nombre` varchar(60) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `idMarca` int(11) NOT NULL,
  `idSeller` int(11) NOT NULL,
  `Precio` float(10,2) NOT NULL,
  `Talla` double(3,1) NOT NULL,
  `img` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Descripcion` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `Materiales` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `disponibilidad` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rieles`
--

INSERT INTO `rieles` (`id`, `Nombre`, `idMarca`, `idSeller`, `Precio`, `Talla`, `img`, `Descripcion`, `Materiales`, `disponibilidad`) VALUES
(1, 'Air Max 97 Have A Nike Day Indigo Storm', 1, 1, 280.00, 9.5, '../img/97_nikeDay.jpg', 'Uno de los iconos de Nike se viste de gala con la ediciï¿½n Have A Nike Day, donde el swoosh es la sonrisa de todos al ponernos este par.', 'Gamuza, cuero y malla', 5),
(2, 'Nike Jordan 1 Retro High Satin Black Toe', 10, 2, 480.00, 9.5, '../img/jordan1.jpg', 'Las Air Jordan 1 High \"Bred\" fueron las primeras que salieron a la venta y las que mï¿½s expectaciï¿½n crearon incluso antes del lanzamiento oficial debido a una carta de aviso que la NBA.', '-Parte superior: cuero<br />\r\n-Forro: cuero <br />\r\n-suela: goma x air ', 1),
(3, 'Nike Air Max 720 University Red Blue Fury', 1, 1, 129.00, 9.5, '../img/720_rbf.jpg', 'Este modelo es una secuela directa de uno de los modelos mï¿½s vendidos a nivel mundial el aï¿½o pasado, la Air Max 270, que hasta ese entonces tenï¿½a el rï¿½cord de la mayor cï¿½psula de amortiguaci', '-Parte superior: Gamuza/Textil.<br />\r\n-Forro: Textil.<br />\r\n-Suela:Caucho. ', 2),
(4, 'Nike React Element 55 White Psychic Purple Hyper Pink', 1, 1, 109.00, 10.0, '../img/react_wpphp.jpg', 'Las Element 87 son las hermanas mayores de las Nike React Element 55, y algunos cambios quedan a la vista. Entre ellos el upper translúcido de las 87 pasa a ser un upper opaco fabricado en malla.\r\n', '<b>-Parte superior:</b> Textil.\r\n                        <br>\r\n                        <b>-Forro:</b> Textil.\r\n                        <br>\r\n                        <b>-Suela:</b> Textil.', 2),
(5, 'Nike React Element 55 White Psychic Purple Hyper Pink', 1, 2, 109.00, 9.5, '../img/react_wpphp.jpg', 'Las Element 87 son las hermanas mayores de las Nike React Element 55, y algunos cambios quedan a la vista. Entre ellos el upper translúcido de las 87 pasa a ser un upper opaco fabricado en malla.\r\n', '<b>-Parte superior:</b> Textil.\r\n                        <br>\r\n                        <b>-Forro:</b> Textil.\r\n                        <br>\r\n                        <b>-Suela:</b> Textil.', 5),
(6, 'adidas Yeezy Boost 350 V2 Clay', 2, 2, 218.00, 9.5, '../img/yzb350clay.jpg', 'Colaboración entre adidas y el rapero Kanye West. Esta es la re-invensión del módelo original con colorway arcilla.', '<b>-Parte superior:</b> Textil.\r\n                        <br>\r\n                        <b>-Forro:</b> Textil.\r\n                        <br>\r\n                        <b>-Suela:</b> Caucho.', 0),
(7, 'adidas Yeezy Boost 700 Wave Runner Solid Grey', 2, 2, 400.00, 10.0, '../img/yzb700rsg.jpg', 'Colaboración entre el rapero Kanye West y adidas. Esta vez se retoma el modelo Wave Runner del año 1985 y es adaptado a la nueva tendecia de sneakers Chunky o robustos.', '<b>-Parte superior:</b> Textil.\r\n                        <br>\r\n                        <b>-Forro:</b> Textil.\r\n                        <br>\r\n                        <b>-Suela:</b> Caucho.', 0),
(9, 'Kyrie 5 Spongebob Patrick', 1, 26, 180.00, 9.5, '../img/kyrie_spP.jpg', 'DiseÃ±ados por el judador de baloncesto Kyrie Irnving, en su versiÃ³n 5 ofrece tracciÃ³n para dribalr y una unidad de Zoom Air turbo. Esta presentaciÃ³n es una colaboraciÃ³n con Nickelodeon y Bob Espo', '-Parte superior: Textil.\r\n-Forro: Textil.\r\n-Suela: Caucho.', 5),
(10, 'Kyrie 5 Spongebob Patrick', 1, 13, 180.00, 10.0, '../img/kyrie_spP.jpg', 'DiseÃƒÂ±ados por el judador de baloncesto Kyrie Irnving, en su versiÃƒÂ³n 5 ofrece tracciÃƒÂ³n para dribalr y una unidad de Zoom Air turbo. Esta presentaciÃƒÂ³n es una colaboraciÃƒÂ³n con Nickelodeon.', '-Parte superior: Textil.\r\n-Forro: Textil.\r\n-Suela: Caucho.', 5),
(15, 'Air Jordan 1 Retro High Off-White Chicago', 10, 26, 5218.00, 9.5, '../img/jordan1_offWhiteChicago.jpg', 'Las Air Jordan 1 por Off-White hace referencia a la reinvenciÃ³n de la misma silueta. Realizado por el diseÃ±ador Virgil Abloh para las: \"The ten\" de Off-White y Nike.', '-Parte superior: Gamuza/Textil.\r\n-Forro: Textil/Cuero.\r\n-Suela: Caucho.', 1),
(16, 'adidas Kamanda Dragon Ball Z Majin Buu', 2, 26, 250.00, 9.5, '../img/dgbz_Majinbu.jpg', 'ColaboraciÃ³n con Dragon Ball Z. Esta silueta nacida en 2018 es una reinvenciÃ³n de los modelos clasicos de adiad, con una media suela exorbitante tamada de la suela de los adidas samba este snekar es', '-Parte superior: Gamuza/Textil.\r\n-Forro: Textil/Cuero.\r\n-Suela: Caucho.', 2),
(17, 'Air Fear Of God 1 Triple Black', 1, 26, 554.99, 9.5, '../img/airfear.jpg', 'La colaboraciÃ³n entre Jerry Lorenzo y la marca del Swoosh que tanto revuelo generÃ³ a finales del 2019 regresa con estos Air Fear of God en un colorway Triple Black', '-Upper: Malla <br>\r\n-Media suela: caucho granulado con capsula air <br>\r\n', 4),
(18, 'Air Fear of God', 1, 32, 756.00, 9.5, '../img/airfear.jpg', 'chghgfjgf', 'gjfg', 8),
(19, 'Air Fear of God', 12, 33, 578.00, 9.5, '../img/airfear.jpg', 'bvjv', 'khgkjh', 5);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indices de la tabla `compra`
--
ALTER TABLE `compra`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idCliente` (`idCliente`);

--
-- Indices de la tabla `detalle_compra`
--
ALTER TABLE `detalle_compra`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_idCompra` (`idCompra`),
  ADD KEY `idRiel` (`idRiel`);

--
-- Indices de la tabla `marcas`
--
ALTER TABLE `marcas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rieles`
--
ALTER TABLE `rieles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idMarca` (`idMarca`),
  ADD KEY `idSeller` (`idSeller`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT de la tabla `compra`
--
ALTER TABLE `compra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT de la tabla `detalle_compra`
--
ALTER TABLE `detalle_compra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de la tabla `marcas`
--
ALTER TABLE `marcas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `rieles`
--
ALTER TABLE `rieles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `compra`
--
ALTER TABLE `compra`
  ADD CONSTRAINT `compra_ibfk_2` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`id`);

--
-- Filtros para la tabla `detalle_compra`
--
ALTER TABLE `detalle_compra`
  ADD CONSTRAINT `detalle_compra_ibfk_1` FOREIGN KEY (`idRiel`) REFERENCES `rieles` (`id`),
  ADD CONSTRAINT `detalle_compra_ibfk_2` FOREIGN KEY (`idCompra`) REFERENCES `compra` (`id`);

--
-- Filtros para la tabla `rieles`
--
ALTER TABLE `rieles`
  ADD CONSTRAINT `rieles_ibfk_1` FOREIGN KEY (`idMarca`) REFERENCES `marcas` (`id`),
  ADD CONSTRAINT `rieles_ibfk_2` FOREIGN KEY (`idSeller`) REFERENCES `cliente` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
